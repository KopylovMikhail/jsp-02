package ru.kopylov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.api.endpoint.ProjectDto;
import ru.kopylov.tm.api.endpoint.TaskDto;

import java.util.List;

public final class CommandUtil {

    public static void printProjectList(@NotNull List<ProjectDto> projectList) {
        System.out.println("[PROJECT LIST]");
        int count = 1;
        for (@NotNull final ProjectDto project : projectList) {
            System.out.println(count++ + ". " + project.getName());
        }
    }

    public static void printTaskList(@NotNull List<TaskDto> taskList) {
        System.out.println("[TASK LIST]");
        int count = 1;
        for (final TaskDto task : taskList) {
            System.out.println(count++ + ". " + task.getName());
        }
    }

    public static void printProjectListWithParam(@NotNull List<ProjectDto> projectList) {
        System.out.println("[PROJECT LIST]");
        int count = 1;
        for (@NotNull final ProjectDto project : projectList) {
            System.out.println(count++ + ". " + project.getName() +
                    "; description: " + project.getDescription() +
                    "; date start: " + DateUtil.xgcToString(project.getDateStart()) +
                    "; date finish: " + DateUtil.xgcToString(project.getDateFinish()) +
                    "; state: " + project.getState());
        }
    }

    public static void printTaskListWithParam(@NotNull List<TaskDto> taskList) {
        System.out.println("[TASK LIST]");
        int count = 1;
        for (final TaskDto task : taskList) {
            System.out.println(count++ + ". " + task.getName() +
                    "; description: " + task.getDescription() +
                    "; date start: " + DateUtil.xgcToString(task.getDateStart()) +
                    "; date finish: " + DateUtil.xgcToString(task.getDateFinish()) +
                    "; project id: " + task.getProjectId() +
                    "; state: " + task.getState());
        }
    }

}
