package ru.kopylov.tm.command.data.xml;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class SaveFasterxmlXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-xml-save";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saving a subject area using FASTERXML in xml-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML by FASTERXML SAVE]");
        dataEndpoint.saveDataXml(bootstrap.getToken());
        System.out.println("[OK]");
    }

}
