package ru.kopylov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.api.endpoint.IDataEndpoint;
import ru.kopylov.tm.api.endpoint.IProjectEndpoint;
import ru.kopylov.tm.api.endpoint.ITaskEndpoint;
import ru.kopylov.tm.api.endpoint.IUserEndpoint;
import ru.kopylov.tm.context.Bootstrap;

@Component
@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    @Autowired
    protected Bootstrap bootstrap;

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    protected IDataEndpoint dataEndpoint;

//    @NotNull
//    protected Bootstrap bootstrap = new Bootstrap();
//
//    public AbstractCommand(@NotNull final Bootstrap bootstrap) {
//        this.bootstrap = bootstrap;
//    }
//
//    public void setBootstrap(@NotNull final Bootstrap bootstrap) {
//        this.bootstrap = bootstrap;
//    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

}
