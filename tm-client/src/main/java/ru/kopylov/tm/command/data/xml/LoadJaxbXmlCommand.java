package ru.kopylov.tm.command.data.xml;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class LoadJaxbXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-xml-load-jaxb";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load a subject area using JAX-B from xml-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML by JAX-B LOAD]");
        dataEndpoint.loadDataXmlJaxb(bootstrap.getToken());
        System.out.println("[OK]");
    }

}
