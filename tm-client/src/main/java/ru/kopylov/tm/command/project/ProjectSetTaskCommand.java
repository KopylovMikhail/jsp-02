package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.endpoint.ProjectDto;
import ru.kopylov.tm.api.endpoint.TaskDto;
import ru.kopylov.tm.util.CommandUtil;

import java.util.List;

@Component
@NoArgsConstructor
public final class ProjectSetTaskCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-set-task";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Assign a task to a project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SET PROJECT'S TASK]");
        @NotNull final List<ProjectDto> projects = projectEndpoint.getProjectList(bootstrap.getToken(), null);
        CommandUtil.printProjectListWithParam(projects);
        System.out.println("ENTER EXISTING PROJECT NUMBER:");
        @Nullable String terminalCommand = bootstrap.getTerminalService().getReadLine();
        if (terminalCommand == null || terminalCommand.isEmpty()) {
            System.out.println("Number is empty.");
            return;
        }
        @NotNull final Integer projectNumber = Integer.parseInt(terminalCommand);
        @NotNull final List<TaskDto> tasks = taskEndpoint.getTaskList(bootstrap.getToken(), null);
        CommandUtil.printTaskListWithParam(tasks);
        System.out.println("ENTER EXISTING TASK NUMBER:");
        terminalCommand = bootstrap.getTerminalService().getReadLine();
        if (terminalCommand == null || terminalCommand.isEmpty()) {
            System.out.println("Number is empty.");
            return;
        }
        @NotNull final Integer taskNumber = Integer.parseInt(terminalCommand);
        final boolean setSuccess = projectEndpoint
                .setProjectTask(bootstrap.getToken(), projectNumber, taskNumber);
        if (setSuccess)
            System.out.println("[OK]\n");
        else System.out.println("Such a project/task does not exist or name is empty.");
    }

}
