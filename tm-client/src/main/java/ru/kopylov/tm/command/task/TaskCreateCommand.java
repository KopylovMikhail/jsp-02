package ru.kopylov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]\n" +
                "ENTER NAME:");
        @Nullable final String taskName = bootstrap.getTerminalService().getReadLine();
        final boolean createSuccess = taskEndpoint.createTask(bootstrap.getToken(), taskName);
        if (createSuccess) System.out.println("[OK]\n");
        else System.out.println("Such a task exists or name is empty.");
    }

}
