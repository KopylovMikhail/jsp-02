package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class UserUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update user password.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER PASSWORD UPDATE]\n" +
                "ENTER NEW LOGIN:");
        @Nullable final String newLogin = bootstrap.getTerminalService().getReadLine();
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String newPassword = bootstrap.getTerminalService().getReadLine();
        if (newLogin == null || newLogin.isEmpty() || newPassword == null || newPassword.isEmpty()) {
            System.out.println("LOGIN OR PASSWORD IS EMPTY.\n");
            return;
        }
        @Nullable String token = bootstrap.getToken();
        userEndpoint.updateUser(token, newLogin, newPassword);
        System.out.println("[PASSWORD UPDATE SUCCESS]\n");
    }

}
