package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.endpoint.UserDto;

@Component
@NoArgsConstructor
public final class UserMyProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "my-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show current user profile.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final UserDto user = userEndpoint.getUserProfile(bootstrap.getToken());
        if (user == null) {
            System.out.println("You need login.");
            return;
        }
        System.out.println("[USER PROFILE]\n" + "login: " + user.getLogin() + ", role: " + user.getRole());
    }

}
