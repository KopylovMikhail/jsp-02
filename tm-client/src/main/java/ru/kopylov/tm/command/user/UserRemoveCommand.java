package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public class UserRemoveCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "user-remove";
    }

    @Override
    public @NotNull String getDescription() {
        return "Remove selected user. Only for administrator.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER EXISTING USER ID:");
        @Nullable final String userId = bootstrap.getTerminalService().getReadLine();
        if (userId == null || userId.isEmpty()) {
            System.out.println("Id is empty.");
            return;
        }
        userEndpoint.removeUser(bootstrap.getToken(), userId);
        System.out.println("[OK]");
    }

}
