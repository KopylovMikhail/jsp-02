package ru.kopylov.tm.command.data.json;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class SaveFasterxmlJsonCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-json-save";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saving a subject area using FASTERXML in json-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON by FASTERXML SAVE]");
        dataEndpoint.saveDataJson(bootstrap.getToken());
        System.out.println("[OK]");
    }

}
