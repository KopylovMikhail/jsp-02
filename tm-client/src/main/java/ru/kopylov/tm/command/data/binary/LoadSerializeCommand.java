package ru.kopylov.tm.command.data.binary;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class LoadSerializeCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "data-bin-load";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load a subject area using serialization.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN LOAD]");
        dataEndpoint.loadDataBin(bootstrap.getToken());
        System.out.println("[OK]");
    }

}
