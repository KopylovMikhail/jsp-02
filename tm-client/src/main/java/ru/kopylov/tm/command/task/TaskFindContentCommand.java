package ru.kopylov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.endpoint.TaskDto;
import ru.kopylov.tm.util.CommandUtil;

import java.util.List;

@Component
@NoArgsConstructor
public final class TaskFindContentCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "task-find";
    }

    @Override
    public @NotNull String getDescription() {
        return "Shows all tasks that contain the search word in the name or description.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS FIND]" +
                "\nENTER THE WORD:");
        @Nullable final String findWord = bootstrap.getTerminalService().getReadLine();
        @NotNull final List<TaskDto> tasks = taskEndpoint.findTaskContent(bootstrap.getToken(), findWord);
        CommandUtil.printTaskListWithParam(tasks);
    }

}
