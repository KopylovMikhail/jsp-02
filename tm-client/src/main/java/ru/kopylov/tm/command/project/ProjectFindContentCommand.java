package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.endpoint.ProjectDto;
import ru.kopylov.tm.util.CommandUtil;

import java.util.List;

@Component
@NoArgsConstructor
public final class ProjectFindContentCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "project-find";
    }

    @Override
    public @NotNull String getDescription() {
        return "Shows all projects that contain the search word in the name or description.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS FIND]" +
                "\nENTER THE WORD:");
        @Nullable final String findWord = bootstrap.getTerminalService().getReadLine();
        @NotNull final List<ProjectDto> projects = projectEndpoint.findProjectContent(bootstrap.getToken(), findWord);
        CommandUtil.printProjectListWithParam(projects);
    }

}
