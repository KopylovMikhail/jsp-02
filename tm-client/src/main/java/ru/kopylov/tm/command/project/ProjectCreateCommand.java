package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return  "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]\n" +
                "ENTER NAME:");
        @Nullable final String projectName = bootstrap.getTerminalService().getReadLine();
        final boolean createSuccess = projectEndpoint.createProject(bootstrap.getToken(), projectName);
        if (createSuccess) System.out.println("[OK]\n");
        else System.out.println("Such a project exists or name is empty.");
    }

}
