package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class UserRegistrationCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "registration";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User registration.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REGISTRATION]\n" +
                "ENTER LOGIN:");
        @Nullable final String login = bootstrap.getTerminalService().getReadLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = bootstrap.getTerminalService().getReadLine();
        if (password == null || password.isEmpty() || login == null || login.isEmpty()) {
            System.out.println("LOGIN OR PASSWORD IS EMPTY.\n");
            return;
        }
        if (!userEndpoint.persistUser(login, password)) {
            System.out.println("Such user already exist or login/password is empty");
            return;
        }
        System.out.println("[REGISTRATION SUCCESS]");
    }

}
