package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.endpoint.ProjectDto;
import ru.kopylov.tm.util.CommandUtil;

import java.util.List;

@Component
@NoArgsConstructor
public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        System.out.println("\nFor sort list type any of these commands:" +
                "\n-     create-date" +
                "\n-     start-date" +
                "\n-     finish-date" +
                "\n-     state" +
                "\npress [Enter] for default sort.");
        @Nullable final String typeSort = bootstrap.getTerminalService().getReadLine();
        @Nullable final List<ProjectDto> projects = projectEndpoint.getProjectList(bootstrap.getToken(), typeSort);
        if (projects == null) {
            System.out.println("Command not exist.");
            return;
        }
        CommandUtil.printProjectListWithParam(projects);
    }

}
