package ru.kopylov.tm.command.data.xml;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class SaveJaxbXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-xml-save-jaxb";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saving a subject area using JAX-B in xml-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML by JAX-B SAVE]");
        dataEndpoint.saveDataXmlJaxb(bootstrap.getToken());
        System.out.println("[OK]");
    }

}
