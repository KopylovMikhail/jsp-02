package ru.kopylov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.config.ApplicationConfig;
import ru.kopylov.tm.context.Bootstrap;

import java.util.Collection;

public final class Application {

    public static void main(String[] args) throws Exception {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        @NotNull final Collection<AbstractCommand> commands = context.getBeansOfType(AbstractCommand.class).values();
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init(commands);
    }

}
