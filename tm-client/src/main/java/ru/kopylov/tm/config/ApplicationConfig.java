package ru.kopylov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.kopylov.tm.api.endpoint.IDataEndpoint;
import ru.kopylov.tm.api.endpoint.IProjectEndpoint;
import ru.kopylov.tm.api.endpoint.ITaskEndpoint;
import ru.kopylov.tm.api.endpoint.IUserEndpoint;
import ru.kopylov.tm.endpoint.DataEndpointService;
import ru.kopylov.tm.endpoint.ProjectEndpointService;
import ru.kopylov.tm.endpoint.TaskEndpointService;
import ru.kopylov.tm.endpoint.UserEndpointService;

@Configuration
@ComponentScan("ru.kopylov.tm")
public class ApplicationConfig {

    @Bean
    @NotNull
    public IUserEndpoint userEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public IDataEndpoint dataEndpoint() {
        return new DataEndpointService().getDataEndpointPort();
    }

}
