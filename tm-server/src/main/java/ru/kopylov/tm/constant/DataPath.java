package ru.kopylov.tm.constant;

import org.jetbrains.annotations.NotNull;

import java.io.File;

public final class DataPath {

    @NotNull
    private static final String userDir = System.getProperty("user.dir");

    @NotNull
    public static final String PATH_BIN = userDir + File.separator + "data.bin";

    @NotNull
    public static final String PATH_FASTERXML_JSON = userDir + File.separator + "data.json";

    @NotNull
    public static final String PATH_FASTERXML_XML = userDir + File.separator + "data.xml";

    @NotNull
    public static final String PATH_JAXB_JSON = userDir + File.separator + "data-jaxb.json";

    @NotNull
    public static final String PATH_JAXB_XML = userDir + File.separator + "data-jaxb.xml";

}
