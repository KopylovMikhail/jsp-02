package ru.kopylov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kopylov.tm.api.endpoint.IDataEndpoint;
import ru.kopylov.tm.api.endpoint.IProjectEndpoint;
import ru.kopylov.tm.api.endpoint.ITaskEndpoint;
import ru.kopylov.tm.api.endpoint.IUserEndpoint;
import ru.kopylov.tm.api.service.IPropertyService;
import ru.kopylov.tm.api.service.ITerminalService;

import javax.xml.ws.Endpoint;

@Getter
@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IDataEndpoint dataEndpoint;

    @NotNull
    @Autowired
    private ITerminalService terminalService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    public void init() {
//        propertyService.init();
        endpointRegistry();
    }

    private void endpointRegistry() {
        @NotNull final String host = propertyService.getHost();
        @NotNull final String port = propertyService.getPort();
        @NotNull final String url = String.format("http://%s:%s/", host, port);
        Endpoint.publish(url + userEndpoint.getUrl(), userEndpoint);
        System.out.println(url + userEndpoint.getUrl());
        Endpoint.publish(url + projectEndpoint.getUrl(), projectEndpoint);
        System.out.println(url + projectEndpoint.getUrl());
        Endpoint.publish(url + taskEndpoint.getUrl(), taskEndpoint);
        System.out.println(url + taskEndpoint.getUrl());
        Endpoint.publish(url + dataEndpoint.getUrl(), dataEndpoint);
        System.out.println(url + dataEndpoint.getUrl());

    }

}
