package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kopylov.tm.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    //<S extends T> S save(S entity)

    //void delete(T entity)

    void deleteById(@NotNull String id);

//    @NotNull
//    Optional<User> findById(@NotNull String id);

    @NotNull
    User findByLoginAndPassword(@NotNull String login, @NotNull String password);

    //List<T>	findAll()

}
