package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kopylov.tm.entity.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllByUserIdOrderByDateStart(@NotNull String userId);

    @NotNull
    List<Task> findAllByUserIdOrderByDateFinish(@NotNull String userId);

    @NotNull
    List<Task> findAllByUserIdOrderByState(@NotNull String userId);

    void removeAllByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllByNameContainsOrDescriptionContains(@NotNull String content1, @NotNull String content2);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String projectId);

}
