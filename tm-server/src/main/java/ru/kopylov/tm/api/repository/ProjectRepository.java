package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kopylov.tm.entity.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserIdOrderByDateStart(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserIdOrderByDateFinish(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserIdOrderByState(@NotNull String userId);

    void removeAllByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAllByNameContainsOrDescriptionContains(@NotNull String content1, @NotNull String content2);

}
