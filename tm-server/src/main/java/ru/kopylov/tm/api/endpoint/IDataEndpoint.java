package ru.kopylov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDataEndpoint {

    @NotNull
    String getUrl();

    @WebMethod
    void saveDataBin(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    void loadDataBin(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    void saveDataJson(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    void loadDataJson(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    void saveDataJsonJaxb(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    void loadDataJsonJaxb(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    void saveDataXml(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    void loadDataXml(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    void saveDataXmlJaxb(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    void loadDataXmlJaxb(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

}
