package ru.kopylov.tm.api.service;

public interface IDataService {

    void saveDataBin() throws Exception;

    void loadDataBin() throws Exception;

    void saveDataJson() throws Exception;

    void loadDataJson() throws Exception;

    void saveDataJsonJaxb() throws Exception;

    void loadDataJsonJaxb() throws Exception;

    void saveDataXml() throws Exception;

    void loadDataXml() throws Exception;

    void saveDataXmlJaxb() throws Exception;

    void loadDataXmlJaxb() throws Exception;

}
