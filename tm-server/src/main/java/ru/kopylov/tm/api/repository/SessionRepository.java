package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kopylov.tm.entity.Session;

@Repository
public interface SessionRepository extends JpaRepository<Session, String> {

    void removeByUserId(@NotNull String userId);

}
