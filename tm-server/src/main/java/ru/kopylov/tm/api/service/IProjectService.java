package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;

import java.util.List;

public interface IProjectService {

    boolean persist(@Nullable Project project) throws Exception;

    boolean persist(@Nullable String currentUserId, @Nullable String projectName) throws Exception;

    @NotNull
    List<Project> findAll() throws Exception;

    @NotNull
    List<Project> findAll(String currentUserId) throws Exception;

    boolean merge(@Nullable Project project) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable String currentUserId, @Nullable String typeSort) throws Exception;

    boolean remove(@Nullable String projectId) throws Exception;

    boolean merge(
            @Nullable String currentUserId,
            @NotNull Integer projectNumber,
            @Nullable String projectName,
            @Nullable String projectDescription,
            @Nullable String projectDateStart,
            @Nullable String projectDateFinish,
            @Nullable Integer stateNumber
    ) throws Exception;

//    void removeAll() throws Exception;

    boolean remove(@Nullable String currentUserId, @NotNull Integer projectNumber) throws Exception;

    void removeAll(@Nullable String currentUserId) throws Exception;

    boolean setTask(
            @Nullable String currentUserId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws Exception;

    boolean setTask(
            @Nullable String currentUserId,
            @NotNull Integer projectNumber,
            @NotNull Integer taskNumber
    ) throws Exception;

    @NotNull
    List<Task> getTaskList(@Nullable String currentUserId, @Nullable String projectName) throws Exception;

    @NotNull
    List<Task> getTaskList(@Nullable String currentUserId, @NotNull Integer projectNumber) throws Exception;

    @NotNull
    List<Project> findByContent(@Nullable String content) throws Exception;

}
