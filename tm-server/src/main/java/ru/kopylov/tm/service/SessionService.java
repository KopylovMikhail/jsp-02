package ru.kopylov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kopylov.tm.api.repository.SessionRepository;
import ru.kopylov.tm.api.service.IPropertyService;
import ru.kopylov.tm.api.service.ISessionService;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.util.SignatureUtil;
import ru.kopylov.tm.util.TokenUtil;

@Getter
@Service
@Transactional
@NoArgsConstructor
public class SessionService extends AbstractService implements ISessionService {

    @NotNull
    @Autowired
    private SessionRepository sessionRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    public boolean remove(@Nullable final String sessionId) {
        if (sessionId == null || sessionId.isEmpty()) return false;
        sessionRepository.deleteById(sessionId);
        return true;
    }

    public boolean removeByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return false;
        sessionRepository.removeByUserId(userId);
        return true;
    }

    public boolean persist(@Nullable final Session session) {
        if (session == null) return false;
        if (session.getId() == null) return false;
        sessionRepository.save(session);
        return true;
    }

    @Nullable
    public Session persist(@Nullable final User user) {
        if (user == null) return null;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return null;
        @NotNull final Session session = new Session();
        session.setUser(user);
        @Nullable final String sessionSignature = SignatureUtil
                .sign(session, propertyService.getSalt(), propertyService.getCycle());
        session.setSignature(sessionSignature);
        sessionRepository.save(session);
        return session;
    }

    @Nullable
    public Session findOne(@Nullable final String sessionId) {
        if (sessionId == null || sessionId.isEmpty()) return null;
        return sessionRepository.findById(sessionId).orElse(null);
    }

    public void validate(@Nullable final Session clientSession) throws Exception {
        if (
                clientSession == null ||
                clientSession.getSignature() == null ||
                clientSession.getTimestamp() == null ||
                clientSession.getUser() == null ||
                clientSession.getUser().getId() == null
        ) throw new Exception("User is not logged in.");
        @Nullable final Session serverSession = findOne(clientSession.getId());
        if (serverSession == null) throw new Exception("Session does not exist.");
        if (!clientSession.getSignature().equals(serverSession.getSignature()))
            throw new Exception("Session is not valid.");
        clientSession.setSignature(null);
        @Nullable final String clientSignature = SignatureUtil
                .sign(clientSession, propertyService.getSalt(), propertyService.getCycle());
        if (!serverSession.getSignature().equals(clientSignature))
            throw new Exception("Session is not valid.");
        final long timeDifference = System.currentTimeMillis() - clientSession.getTimestamp();
        if (timeDifference > propertyService.getLifeTime()) throw new Exception("Session expired.");
    }

    @Nullable
    public String encryptToken(@Nullable final Session session) throws Exception {
        if (session == null) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String plainJson = objectMapper.writeValueAsString(session);
        return TokenUtil.encrypt(plainJson, propertyService.getSecretKey());
    }

    @Nullable
    public Session decryptToken(@Nullable final String token) throws Exception {
        if (token == null || token.isEmpty()) return null;
        @NotNull String encodedJson = TokenUtil.decrypt(token, propertyService.getSecretKey());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(encodedJson, Session.class);
        return session;
    }

}
