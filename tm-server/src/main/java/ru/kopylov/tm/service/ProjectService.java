package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kopylov.tm.api.repository.ProjectRepository;
import ru.kopylov.tm.api.service.IProjectService;
import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.api.service.IUserService;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.enumerated.TypeSort;
import ru.kopylov.tm.util.DateUtil;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
@Transactional
@NoArgsConstructor
public class ProjectService extends AbstractService implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private IUserService userService;

    @Autowired
    private ITaskService taskService;

    public boolean persist(@Nullable final Project project) {
        if (project == null) return false;
        if (project.getId() == null) return false;
        projectRepository.save(project);
        return true;
    }

    public boolean persist(@Nullable final String currentUserId, @Nullable final String projectName) throws Exception {
        @NotNull final Project project = new Project();
        project.setName(projectName);
        @Nullable final User user = userService.findOne(currentUserId);
        project.setUser(user);
        project.setDateStart(new Date());
        project.setDateFinish(new Date());
        return persist(project);
    }

    @NotNull
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    public List<Project> findAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        return projectRepository.findAllByUserId(currentUserId);
    }

    @Nullable
    public List<Project> findAll(@Nullable final String currentUserId, @Nullable final String typeSort) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        if (TypeSort.CREATE_DATE.getDisplayName().equals(typeSort) || typeSort == null || typeSort.isEmpty())
            return projectRepository.findAllByUserId(currentUserId);
        if (TypeSort.START_DATE.getDisplayName().equals(typeSort)) {
            return projectRepository.findAllByUserIdOrderByDateStart(currentUserId);
        }
        if (TypeSort.FINISH_DATE.getDisplayName().equals(typeSort)) {
            return projectRepository.findAllByUserIdOrderByDateFinish(currentUserId);
        }
        if (TypeSort.STATE.getDisplayName().equals(typeSort)) {
            return projectRepository.findAllByUserIdOrderByState(currentUserId);
        }
        return null;
    }

    public boolean merge(@Nullable final Project project) {
        if (project == null) return false;
        if (project.getId() == null || project.getId().isEmpty()) return false;
        projectRepository.save(project);
        return true;
    }

    public boolean merge(
            @Nullable final String currentUserId,
            @NotNull final Integer projectNumber,
            @Nullable final String projectName,
            @Nullable final String projectDescription,
            @Nullable final String projectDateStart,
            @Nullable final String projectDateFinish,
            @Nullable final Integer stateNumber
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final List<Project> projectList;
        projectList = findAll(currentUserId);
        if (projectNumber < 1 || projectNumber > projectList.size()) return false;
        @NotNull final Project project = projectList.get(projectNumber - 1);
        if (projectName != null && !projectName.isEmpty()) project.setName(projectName);
        if (projectDescription != null && !projectDescription.isEmpty())
            project.setDescription(projectDescription);
        if (projectDateStart != null && !projectDateStart.isEmpty()) {
            @NotNull final Date dateStart = DateUtil.stringToDate(projectDateStart);
            project.setDateStart(dateStart);
        }
        if (projectDateFinish != null && !projectDateFinish.isEmpty()) {
            @NotNull final Date dateFinish = DateUtil.stringToDate(projectDateFinish);
            project.setDateFinish(dateFinish);
        }
        if (stateNumber != null) {
            if (stateNumber < 1 || stateNumber > State.values().length) return false;
            project.setState(State.values()[stateNumber-1]);
        }
        return merge(project);
    }

    public boolean remove(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return false;
        projectRepository.deleteById(projectId);
        return true;
    }

    public boolean remove(@Nullable final String currentUserId, @NotNull final Integer projectNumber) {
        @NotNull final List<Project> projectList = findAll(currentUserId);
        if (projectNumber < 1 || projectNumber > projectList.size()) return false;
        @NotNull final Project project = projectList.get(projectNumber - 1);
        return remove(project.getId());
    }

    public void removeAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        projectRepository.removeAllByUserId(currentUserId);
    }

    public boolean setTask(
            @Nullable final String currentUserId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) return false;
        if (taskId == null || taskId.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @Nullable final Project project = projectRepository.findById(projectId).orElse(null);
        @Nullable final Task task = taskService.findOne(taskId);
        if (project == null) return false;
        if (task == null) return false;
        task.setProject(project);
        return taskService.merge(task);
    }

    public boolean setTask(
            @Nullable final String currentUserId,
            @NotNull final Integer projectNumber,
            @NotNull final Integer taskNumber
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final List<Project> projects = findAll(currentUserId);
        @NotNull final List<Task> tasks = taskService.findAll(currentUserId);
        if (projectNumber < 1 || projectNumber > projects.size()) return false;
        if (taskNumber < 1 || taskNumber > tasks.size()) return false;
        @NotNull final Project project = projects.get(projectNumber - 1);
        @NotNull final Task task = tasks.get(taskNumber - 1);
        task.setProject(project);
        return taskService.merge(task);
    }

    @NotNull
    public List<Task> getTaskList(
            @Nullable final String currentUserId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        return taskService.findAllByProjectId(projectId);
    }

    @NotNull
    public List<Task> getTaskList(
            @Nullable final String currentUserId,
            @NotNull final Integer projectNumber
    ) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        @NotNull final List<Project> projects = findAll(currentUserId);
        if (projectNumber < 1 || projectNumber > projects.size()) return Collections.emptyList();
        @NotNull final Project project = projects.get(projectNumber - 1);
        return getTaskList(currentUserId, project.getId());
    }

    @NotNull
    public List<Project> findByContent(@Nullable String content) {
        if (content == null || content.isEmpty()) return Collections.emptyList();
        return projectRepository.findAllByNameContainsOrDescriptionContains(content, content);
    }

}
