package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> {

    @Nullable
    protected EntityManagerFactory factory;

    @Nullable
    protected EntityManager manager;

}
