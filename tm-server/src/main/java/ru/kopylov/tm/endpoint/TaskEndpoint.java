package ru.kopylov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.kopylov.tm.api.endpoint.ITaskEndpoint;
import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.dto.TaskDto;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.util.EntityToDtoUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.kopylov.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    private final String url = this.getClass().getSimpleName() + "?wsdl";

    @WebMethod
    public void clearTask(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        taskService.removeAll(session.getUser().getId());
    }

    @WebMethod
    public boolean createTask(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "taskName") @Nullable final String taskName
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return taskService.persist(session.getUser().getId(), taskName);
    }

    @NotNull
    @WebMethod
    public List<TaskDto> findTaskContent(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "findWord") @Nullable final String findWord
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return EntityToDtoUtil.getTaskList(taskService.findByContent(findWord));
    }

    @Nullable
    @WebMethod
    public List<TaskDto> getTaskList(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "typeSort") @Nullable final String typeSort
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return EntityToDtoUtil.getTaskList(taskService.findAll(session.getUser().getId(), typeSort));
    }

    @WebMethod
    public boolean removeTask(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "taskNumber") @NotNull final Integer taskNumber
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return taskService.remove(session.getUser().getId(), taskNumber);
    }

    @WebMethod
    public boolean updateTask(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "taskNumber") @NotNull final Integer taskNumber,
            @WebParam(name = "taskName") @Nullable final String taskName,
            @WebParam(name = "taskDescription") @Nullable final String taskDescription,
            @WebParam(name = "taskDateStart") @Nullable final String taskDateStart,
            @WebParam(name = "taskDateFinish") @Nullable final String taskDateFinish,
            @WebParam(name = "stateNumber") @Nullable final Integer stateNumber
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return taskService.merge(
                session.getUser().getId(),
                taskNumber,
                taskName,
                taskDescription,
                taskDateStart,
                taskDateFinish,
                stateNumber
        );
    }

}
