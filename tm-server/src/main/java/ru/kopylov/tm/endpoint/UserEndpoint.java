package ru.kopylov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.kopylov.tm.api.endpoint.IUserEndpoint;
import ru.kopylov.tm.api.service.IUserService;
import ru.kopylov.tm.dto.UserDto;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.util.EntityToDtoUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Setter
@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.kopylov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    private final String url = this.getClass().getSimpleName() + "?wsdl";

    @WebMethod
    public boolean persistUser(
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) throws Exception {
        return userService.persist(login, password);
    }

    @Nullable
    @WebMethod
    public String loginUser(
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) throws Exception {
        @Nullable final User loggedUser = userService.findOne(login, password);
        @Nullable final Session session = sessionService.persist(loggedUser);
        return sessionService.encryptToken(session);
    }

    @WebMethod
    public void logoutUser(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        sessionService.remove(session.getId());
    }

    @Nullable
    @WebMethod
    public UserDto getUserProfile(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        return EntityToDtoUtil.getUser(userService.findOne(session.getUser().getId()));
    }

    @WebMethod
    public void updateUser(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        sessionService.validate(session);
        userService.merge(session.getUser().getId(), login, password);
    }

    @WebMethod
    public void removeUser(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "id") @NotNull final String id
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        userService.remove(id);
    }

    @NotNull
    @WebMethod
    public List<UserDto> getUserList(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        return EntityToDtoUtil.getUserList(userService.findAll());
    }

    @WebMethod
    public boolean removeSession(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "userId") @NotNull final String userId
    ) throws Exception {
        @Nullable final Session session = sessionService.decryptToken(token);
        validate(session);
        return sessionService.removeByUserId(userId);
    }

    private void validate(@Nullable Session session) throws Exception {
        sessionService.validate(session);
        @Nullable final String userId = session.getUser().getId();
        @Nullable User user = userService.findOne(userId);
        if (user == null || user.getRole() != TypeRole.ADMIN) throw new Exception("User does not have enough rights.");
    }

}
